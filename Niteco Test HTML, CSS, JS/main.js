var listCity = [
    {"name" : "Hà Nội"},
    {"name" : "Hà Nam"},
    {"name" : "Hà Mã"},
    {"name" : "Hồ Chí Minh"},
    {"name" : "Nam Định"},
    {"name" : "Bắc Ninh"},
    {"name" : "Bắc Giang"},
    {"name" : "Đà Nẵng"},
    {"name" : "Vũng Tàu"},
    {"name" : "Thừa Thiên Huế"},
    {"name" : "Ninh Bình"}
]

// Funtion search
function keyUpHandler () {
    // getData ()
    var keySearch = document.getElementById('search').value.trim().toLowerCase();
    if(keySearch) {
        let list = listCity.filter(item => {
            if (item.name.toLowerCase().indexOf(keySearch) > -1) {
                return item;
            }
        });
        console.log(list);
        removeDataSearch ();
        if(list[0]) {
            var ul = document.createElement('ul');
            ul.id = 'list-res';
            document.getElementById('search-result').appendChild(ul);
            for( i=0; i < list.length; i++) {
                var item= document.createElement("li")
                item.innerHTML = list[i].name
                item.className = 'item-select'
                document.getElementById('list-res').appendChild(item);
            }
        }
        else {
            document.getElementById('search-result').innerHTML = "Không tìm thấy tỉnh thành nào!";
        }
    }

};

// Delete list previous results
function removeDataSearch () {
    if(document.getElementById('list-res')) {
        document.getElementById('list-res').remove()
    }
}

// End
const search = debounce(keyUpHandler,3000 );

// Function debounce
function debounce(fn, delay, immediate) {
    let timeout;
    return function executedFn() {
        let context = this;
        let args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) fn.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, delay);
        if (callNow) fn.apply(context, args);
    }
}

function toggleMenu () {
    var e = document.getElementById('header')
    e.classList.toggle('open')
}
